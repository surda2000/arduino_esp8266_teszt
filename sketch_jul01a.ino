#include <ESP8266WiFi.h>

const char* ssid = "Plak_guest";
const char* password = "surdaeva";

int ledPin = 16;

int ledPinR = 2;
int ledPinG = 0;
int ledPinB = 4;

WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  pinMode(ledPinR, OUTPUT);
  digitalWrite(ledPinR, HIGH);
  pinMode(ledPinG, OUTPUT);
  digitalWrite(ledPinG, HIGH);
  pinMode(ledPinB, OUTPUT);
  digitalWrite(ledPinB, HIGH);

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");

}

void loop() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  // Wait until the client sends some data
  Serial.println("new client");
  while (!client.available()) {
    delay(1);
  }

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();

  // Match the request

  int value = LOW, valueR = LOW, valueG = LOW, valueB = LOW;
  if (request.indexOf("/LED=OFF") != -1)  {
    digitalWrite(ledPin, HIGH);
    value = HIGH;
  }
  if (request.indexOf("/LED=ON") != -1)  {
    digitalWrite(ledPin, LOW);
    value = LOW;
  }

  if (request.indexOf("/LEDR=OFF") != -1)  {
    digitalWrite(ledPinR, HIGH);
    valueR = HIGH;
  }
  if (request.indexOf("/LEDR=ON") != -1)  {
    digitalWrite(ledPinR, LOW);
    valueR = LOW;
  }
  if (request.indexOf("/LEDG=OFF") != -1)  {
    digitalWrite(ledPinG, HIGH);
    valueG = HIGH;
  }
  if (request.indexOf("/LEDG=ON") != -1)  {
    digitalWrite(ledPinG, LOW);
    valueG = LOW;
  }
  if (request.indexOf("/LEDB=OFF") != -1)  {
    digitalWrite(ledPinB, HIGH);
    valueB = HIGH;
  }
  if (request.indexOf("/LEDB=ON") != -1)  {
    digitalWrite(ledPinB, LOW);
    valueB = LOW;
  }


  // Set ledPin according to the request
  //digitalWrite(ledPin, value);



  // Return the response
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); //  do not forget this one
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("<head>");
  client.println("<meta name='apple-mobile-web-app-capable' content='yes' />");
  client.println("<meta name='apple-mobile-web-app-status-bar-style' content='black-translucent' />");
  client.println("<title>RGB LED control</title>");

  client.println("<style>");
  client.println(".button {");
  client.println("background-color: #4CAF50;");
  client.println("border: none;");
  client.println("color: white;");
  client.println("padding: 15px 25px;");
  client.println("text-align: center;");
  client.println("font-size: 16px;");
  client.println("cursor: pointer;");
  client.println("}");

  client.println(".button:hover {");
  client.println("background-color: green;");
  client.println("}");

  client.println(".slidecontainer {width: 100%;}");

  client.println(".slider{");
  client.println("-webkit-appearance: none;width: 100%;height: 25px;background:#d3d3d3;");
  client.println("outline:none;opacity:0.7; -webkit -transition:.2s;transition: opacity .2s;}");

  client.println(".slider:hover{opacity:1;}");

  client.println(".slider:: -webkit -slider -thumb {");
  client.println("-webkit-appearance:none;appearance:none;width:25px;height:25px;background:#4CAF50;cursor:pointer;}");

  client.println(".slider:: -moz -range -thumb {");
  client.println("width:25px;height:25px;background:#4CAF50;cursor:pointer;}");
  client.println("</style> ");

    client.println("<body bgcolor=\"#a8c0f4\">");

  client.println("</head >");

  client.println("<hr/><hr>");
  client.println("<h4><center> Designed by S </center></h4>");
  client.println("<hr/><hr>");
  client.println("<br><br>");
  client.println("<br><br>");

  client.println("<h1>Setup </h1>");

  client.println("<h2>The Button Element</h2>");
  //  client.println("<button class=\"button\">" Styled Button</button>");

  client.println("<p><a href=\"LEDR=ON\"><button class=\"button\">ON</button></a></p>");

  client.println("<form method=get><br>Red:  <input type='range' min='1' max='100' name=redVal value=redTemp oninput='showValue(this.value)'>");
  client.print("Led pin is now: ");

  if (value == HIGH) {
    client.print("On");
  } else {
    client.print("Off");
  }
  client.println("<br><br>");


  if (valueR == HIGH) {
    client.print("Red On");
  } else {
    client.print("Red Off");
  }
  client.println("<br><br>");

  if (valueG == HIGH) {
    client.print("Green On");
  } else {
    client.print("Green Off");
  }
  client.println("<br><br>");

  if (valueB == HIGH) {
    client.print("Blue On");
  } else {
    client.print("Blue Off");
  }
  client.println("<br><br>");
  client.println("<br><br>");



  client.println("<a href=\"/LED=ON\"\"><button>Turn On </button></a>");
  client.println("<a href=\"/LED=OFF\"\"><button>Turn Off </button></a><br />");
  client.println("<br><br>");

  client.println("<a href=\"/LEDR=ON\"\"><button>Turn On Red</button></a>");
  client.println("<a href=\"/LEDG=ON\"\"><button>Turn On Green</button></a>");
  client.println("<a href=\"/LEDB=ON\"\"><button>Turn On Blue</button></a><br />");
  client.println("<a href=\"/LEDR=OFF\"\"><button>Turn Off Red </button></a>");
  client.println("<a href=\"/LEDG=OFF\"\"><button>Turn Off Green</button></a>");
  client.println("<a href=\"/LEDB=OFF\"\"><button>Turn Off Blue</button></a><br />");



  client.println("<input type = \"range\" min = \"1\" max = \"100\" value = \"50\" class = \"slider\" id = \"myRange\">");



  client.println("<footer>");
  client.println("<p>Made by: Surda</p>");
  client.println("<p>Contact information:");
  client.println("surda2000@tvn.hu</p>");
  client.println("</footer>");

  client.println("</html>");

  delay(1);
  Serial.println("Client disonnected");
  Serial.println("");

}

